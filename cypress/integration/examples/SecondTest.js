describe('MySecondTestSuite', function()
{
    it('Verify Incorrect Login', function() 
    {
      cy.visit('https://www.amazon.in/')
      cy.get('#nav-link-accountList-nav-line-1').click()
      cy.wait(2000)
      cy.get('#ap_email').type('1234567890')
      cy.get('#continue').click()
      cy.wait(2000)
      cy.get('#ap_password').type('123456')
      cy.get('#signInSubmit').click()
     cy.get('.a-alert-heading').should('have.text', 'Important Message!');
    })
  })